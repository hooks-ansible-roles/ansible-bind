BIND
=========

Role to set up a simple BIND server for DNS. Utilizes a dictionary for the hosts and can be tied in with the DHCP role.

Requirements
------------

None.

Role Variables
--------------

    #Domain name
    domain: example.com

    #Reverse domain
    rev_domain: 0.168.192.in-addr.arpa

    #Subnet for systems
    dns_subnet: 192.168.0

    #Dictionary of hosts.
    #Type sets the record type.
    #Last sets the last octet of the address.
    #MAC is optional. The same dictionary can be used with the DHCP role.
    records:
      erl: { type: A, last: 1, mac: }

    #DNS forwarders.
    forwarders:
      - 8.8.8.8
      - 8.8.4.4

Dependencies
------------

None, but can be tied in with the DHCP role.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: dns_servers

      roles:
        - role: ansible-bind
          domain: example.com
          

License
-------

BSD

Author Information
------------------

John Hooks
